package com.bdcsoftware.opencvdetectobject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    ImageView imageView;
    Button testButton;

    Mat mRgba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }

        imageView = (ImageView) findViewById(R.id.testImage);
        testButton = (Button) findViewById(R.id.testButton);

        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                test();
            }
        });

    }

    private void test2(){
        BitmapDrawable d = (BitmapDrawable) getResources().getDrawable(R.drawable.test);
        Bitmap bmp = d.getBitmap();

        //convert bmp to MAT
        Mat rgba = new Mat(bmp.getHeight(), bmp.getWidth(), CvType.CV_8UC4);
        Utils.bitmapToMat(bmp, rgba);

        ColorBlobDetector det = new ColorBlobDetector();
        det.setHsvColor(new Scalar(179,0,0));
        det.process(rgba);

        Utils.matToBitmap(rgba, bmp);

        imageView.setImageBitmap(bmp);
        Log.d(TAG, "Done!");
    }

    private void test(){
        double mMinContourArea = 0.1;

        //decode bmp from Drawables
        BitmapDrawable d = (BitmapDrawable) getResources().getDrawable(R.drawable.test);
        Bitmap bmp = d.getBitmap();

        //convert bmp to MAT
        Mat rgba = new Mat(bmp.getHeight(), bmp.getWidth(), CvType.CV_8UC4);
        Utils.bitmapToMat(bmp, rgba);

        //Imgproc.blur(rgba, rgba, new Size(10,10));
        //Imgproc.dilate(rgba, rgba, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(8, 8)));

        //convert to HSV
        Mat hsv = new Mat();
        Imgproc.cvtColor(rgba, hsv, Imgproc.COLOR_RGB2HSV,3);

        Mat mask = new Mat();
        Core.inRange(hsv, new Scalar(0,0,0), new Scalar(178,255,255), mask);
        Core.bitwise_not(mask, mask);
        Imgproc.erode(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2, 2)));
        Imgproc.erode(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2, 2)));
        Imgproc.dilate(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(16, 16)));


//        Mat dilatedMask = new Mat();
//        Imgproc.dilate(mask, dilatedMask, new Mat());


        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();

        //Imgproc.findContours(mask, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
//
//        // Find max contour area
//        double maxArea = 0;
//        Iterator<MatOfPoint> each = contours.iterator();
//        while (each.hasNext()) {
//            MatOfPoint wrapper = each.next();
//            double area = Imgproc.contourArea(wrapper);
//            if (area > maxArea)
//                maxArea = area;
//        }
//
//
//        // Filter contours by area and resize to fit the original image size
//        contours.clear();
//        each = contours.iterator();
//        while (each.hasNext()) {
//            MatOfPoint contour = each.next();
//            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
//                Core.multiply(contour, new Scalar(4,4), contour);
//                contours.add(contour);
//            }
//        }
//
//        //draw contours
//        for (int i = 0; i < contours.size() ; i++) {
//            Imgproc.drawContours(rgba, contours, i, new Scalar(0,0,255));
//        }

//        ColorBlobDetector det = new ColorBlobDetector();
//        det.setHsvColor(new Scalar(10,255,255));
//        det.process(rgba);


        //Core.bitwise_and(hsv, mask, hsv);
        //split image into channels
        //List<Mat> channels = new ArrayList<Mat>();
        //Core.split(hsv, channels);

        //Core.inRange(channels.get(0), new Scalar(0), new Scalar(10), channels.get(0));

//        //threshold teh H channel
//        Imgproc.threshold(channels.get(0), channels.get(0), -1, 1,
//                Imgproc.THRESH_OTSU);

        //merge channels back into HSV
        //Core.merge(channels, hsv);

        //convert hsv to rgb
        //Imgproc.cvtColor(mask, rgba, Imgproc.COLOR_HSV2RGB,3);

        //convert mat to bmp
        Utils.matToBitmap(mask, bmp);

        imageView.setImageBitmap(bmp);
        Log.d(TAG, "Done!");
    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }

    private Scalar touchSim(){
        Rect touchedRect = new Rect();

        touchedRect.x = 310;
        touchedRect.y = 200;

        touchedRect.width = 40;
        touchedRect.height = 40;

        Mat touchedRegionRgba = mRgba.submat(touchedRect);

        Mat touchedRegionHsv = new Mat();
        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        // Calculate average color of touched region

        Scalar blobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width*touchedRect.height;
        for (int i = 0; i < blobColorHsv.val.length; i++)
            blobColorHsv.val[i] /= pointCount;

//        blobColorHsv = converScalarHsv2Rgba(blobColorHsv);
        return blobColorHsv;
    }


//    public boolean onTouch(View v, MotionEvent event) {
//        int cols = mRgba.cols();
//        int rows = mRgba.rows();
//
//        int xOffset = (getWidth() - cols) / 2;
//        int yOffset = (getHeight() - rows) / 2;
//
//        int x = (int)event.getX() - xOffset;
//        int y = (int)event.getY() - yOffset;
//
//        Log.i(TAG, "Touch image coordinates: (" + x + ", " + y + ")");
//
//        if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return false;
//
//        Rect touchedRect = new Rect();
//
//        touchedRect.x = (x>4) ? x-4 : 0;
//        touchedRect.y = (y>4) ? y-4 : 0;
//
//        touchedRect.width = (x+4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
//        touchedRect.height = (y+4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;
//
//        Mat touchedRegionRgba = mRgba.submat(touchedRect);
//
//        Mat touchedRegionHsv = new Mat();
//        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);
//
//        // Calculate average color of touched region
//        mBlobColorHsv = Core.sumElems(touchedRegionHsv);
//        int pointCount = touchedRect.width*touchedRect.height;
//        for (int i = 0; i < mBlobColorHsv.val.length; i++)
//            mBlobColorHsv.val[i] /= pointCount;
//
//        mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);
//
//        Log.i(TAG, "Touched rgba color: (" + mBlobColorRgba.val[0] + ", " + mBlobColorRgba.val[1] +
//                ", " + mBlobColorRgba.val[2] + ", " + mBlobColorRgba.val[3] + ")");
//
//        mDetector.setHsvColor(mBlobColorHsv);
//
//        Imgproc.resize(mDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);
//
//        mIsColorSelected = true;
//
//        return false; // don't need subsequent touch events
//    }

}
